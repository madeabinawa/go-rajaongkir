-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2020 at 04:06 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lib`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_buku`
--

CREATE TABLE `tb_buku` (
  `id_buku` int(11) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `tahun` char(4) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `gambar` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_buku`
--

INSERT INTO `tb_buku` (`id_buku`, `isbn`, `judul`, `tahun`, `jumlah`, `gambar`) VALUES
(1, '9789791090407', 'CISCO CCNA dan Jaringan Komputers', '2009', 2, 'bukuImage/9789791090407_CISCO_CCNA_dan_Jaringan_Komputers_20190129154954.jpg'),
(5, '9789791090612', 'Web Tips PHP, HTML5 dan CSS3', '2012', 1, 'bukuImage/9789791090612_Web_Tips_PHP,_HTML5_dan_CSS3.jpg'),
(8, '9789792933901', 'Pemrograman Web Dinamis Dengan ASP.Net 4.5', '2012', 1, 'bukuImage/9789792933901_Pemrograman_Web_Dinamis_Dengan_ASP.Net_4.5.jpg'),
(10, '9789797942656', 'Codeigniter Cara Mudah Membangun Aplikasi PHP', '2010', 3, 'bukuImage/9789797942656_Codeigniter_Cara_Mudah_Membangun_Aplikasi_PHP.jpg'),
(22, '9786028655149', 'Kebut Sehari Jadi Master PHP', '2010', 3, 'bukuImage/9786028655149_Kebut_Sehari_Jadi_Master_PHP.jpg'),
(25, '9786020294346', 'Kitab CorelDRAW X8', '2016', 2, 'bukuImage/9786020294346_Kitab_CorelDRAW_X8_20181023162729.jpg'),
(29, '9786020823164', 'Mikrotik Metarouter 100 Illusion', '2016', 1, 'bukuImage/9786020823164_Mikrotik_Metarouter_100_Illusion_20181024053424.jpg'),
(31, '9789798658000', 'Pendidikan Pancasila', '2014', 0, 'bukuImage/9789798658000_Pendidikan_Pancasila_20190128141857.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_buku`
--
ALTER TABLE `tb_buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_buku`
--
ALTER TABLE `tb_buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
