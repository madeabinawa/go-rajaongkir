package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"log"
)

func main() {
	// fmt.Println("Bilangan Random", rand.Intn(10))
	router := mux.NewRouter()
	router.HandleFunc("/cityDetail",getCityDetail).Methods("GET")
	router.HandleFunc("/getBuku",getBuku).Methods("GET")
	log.Fatal(http.ListenAndServe(":9988",router))
}
